package com.gustavo.pagamento.entities;

import java.io.Serializable;

public class Pagamento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private Float pagamentoDiario;
	private Integer dias;
	
	public Pagamento() {
		super();
	}

	public Pagamento(String nome, Float pagamentoDiario, Integer dias) {
		super();
		this.nome = nome;
		this.pagamentoDiario = pagamentoDiario;
		this.dias = dias;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Float getPagamentoDiario() {
		return pagamentoDiario;
	}
	public void setPagamentoDiario(Float pagamentoDiario) {
		this.pagamentoDiario = pagamentoDiario;
	}
	public Integer getDias() {
		return dias;
	}
	public void setDias(Integer dias) {
		this.dias = dias;
	}
	public Float getValorTotal() {
		return dias*pagamentoDiario*8;
	}
	
}
