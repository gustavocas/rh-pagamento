package com.gustavo.pagamento.entities;

import java.io.Serializable;

public class Trabalhador implements Serializable{
	
	public Trabalhador() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Trabalhador(Integer id, String nome, float salario) {
		super();
		this.id = id;
		this.nome = nome;
		this.salario = salario;
	}


	private Integer id;
	private String nome;
	private float salario;
	public long getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getSalario() {
		return salario;
	}
	public void setSalario(float salario) {
		this.salario = salario;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trabalhador other = (Trabalhador) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
