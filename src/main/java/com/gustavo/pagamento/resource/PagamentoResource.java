package com.gustavo.pagamento.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.pagamento.entities.Pagamento;
import com.gustavo.pagamento.services.PagamentoService;

@RestController
@RequestMapping(value="pagamentos")
public class PagamentoResource {

	@Autowired
	private PagamentoService pagamentoService;
	
	
	//exemplo chamada: http://127.0.0.1:8002/pagamentos/1/dias/21
	@GetMapping(value="/{idTrabalhador}/dias/{dias}")
	public ResponseEntity<Pagamento> getPagamento(
			@PathVariable Integer idTrabalhador, 
			@PathVariable Integer dias){
		Pagamento pag = pagamentoService.getPagamento(idTrabalhador, dias);
		
		return ResponseEntity.ok(pag);		
	}
	
	
	//exemplo chamada: http://127.0.0.1:8080/pagamentos/5/dias/21/direto
	@GetMapping(value="/{idTrabalhador}/dias/{dias}/direto")
	public ResponseEntity<Pagamento> getPagamentoDireto(
			@PathVariable Integer idTrabalhador, 
			@PathVariable Integer dias){
		Pagamento pag = pagamentoService.getPagamentoDireto(idTrabalhador, dias);
		
		return ResponseEntity.ok(pag);		
	}
}
