package com.gustavo.pagamento.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.gustavo.pagamento.entities.Pagamento;
import com.gustavo.pagamento.entities.Trabalhador;
import com.gustavo.pagamento.feingClient.TrabalhadorFeingClient;

@Service
public class PagamentoService {
	
	@Autowired
	private TrabalhadorFeingClient trabalhadorFeingClient; 
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	public Pagamento getPagamento(Integer idTrabalhador, Integer diasTrabalhados) {
		
		Trabalhador trab = trabalhadorFeingClient.findById(idTrabalhador).getBody();
		Pagamento pag = new Pagamento(trab.getNome(), trab.getSalario() ,diasTrabalhados);
		return pag;
	}
	
	public Pagamento getPagamentoDireto(Integer idTrabalhador, Integer diasTrabalhados) {
		Map<String, String> parametrosURI = new HashMap<>();
		parametrosURI.put("id", String.valueOf(idTrabalhador));
		
		
		Trabalhador trab = restTemplate.getForObject("http://127.0.0.1:8001/trabalhador/{id}", //esse {id} tem que estar declarado no parametros do Map "id" 
				Trabalhador.class,parametrosURI);
		Pagamento pag = new Pagamento(trab.getNome(), trab.getSalario() ,diasTrabalhados);
		return pag;
	}
}
