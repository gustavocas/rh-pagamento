package com.gustavo.pagamento.config;

/*
 Essa classe cria o template para fazer a chamada direta para o servico em o feingClient
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Configuracoes {

	@Bean
	public RestTemplate getTemplate() {
		return new RestTemplate();
	}
	
	
}
