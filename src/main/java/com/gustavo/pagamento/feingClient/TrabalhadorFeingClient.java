package com.gustavo.pagamento.feingClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gustavo.pagamento.entities.Trabalhador;

@Component
@FeignClient(name="rh-trabalhador", url="", path="/trabalhador/") 
public interface TrabalhadorFeingClient {

	@RequestMapping(method = RequestMethod.GET, value = "/{id}") //@GetMapping(value = "/{id}")
	public ResponseEntity<Trabalhador> findById(@PathVariable Integer id);
}
